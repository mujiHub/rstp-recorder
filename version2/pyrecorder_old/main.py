
#########################################################################

import threading as th  
######################################################################server 

import select
import socket
import time
from recorder import *


GRec = None

def server() -> None:
    host = "127.0.0.1" #socket.gethostname()
    port = 12003

  
    # create a TCP/IP socket
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        sock.setblocking(0)
        # bind the socket to the port
        print("\n host is  ",host)
        sock.bind((host, port))
        # listen for incoming connections
        sock.listen(5)
        print("Server started...")

        # sockets from which we expect to read
        inputs = [sock]
        outputs = []

        while inputs:
            # wait for at least one of the sockets to be ready for processing
            readable, writable, exceptional = select.select(inputs, outputs, inputs)

            for s in readable:
                if s is sock:
                    conn, addr = s.accept()
                    inputs.append(conn)
                else:
                    data = s.recv(1024)
                    if data:
                        print(data)
                        process_msg(  data.decode() )
                    else:
                        inputs.remove(s)
                        s.close()





####################################################################
def process_msg(  p_msg ): 
         global GRec
         print("\n  >>>>>>>>>> \n",p_msg, "\n  >>>>>>>>>> \n")  
         word = p_msg.split(',') 
         if word[0] == "start": 
          print("\n Start")      
          port  = word[1].strip() 
          vlength  = word[2].strip()
          if word[3]:          
           label_string  = word[3].strip()        
          if GRec is None:           
            GRec = CRecord()
          GRec.record(port, vlength, label_string)
        
          return


         elif word[0] == "stop": 
            print("\n Stop")
            if GRec:
             GRec.stop()
             return
         elif word[0]:
          print("\n  this is C++ \n"+p_msg)    


if __name__ == "__main__": 
    server()           
                  