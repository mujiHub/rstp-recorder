import os
import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst
from threading import Timer
import json
from datetime import datetime




REC_STATE = False


def callback(p_self, cam_id):
    print("stop called")
    p_self.pause(cam_id) 
   

class CRecord():

    

    def __init__(self):

        Gst.init(None)
        self.cam_dict = {}
        self.Recordings_dict ={}
        self.Cam_obj_dict ={}
        self.load_cams()
        

       
        # parse file
      
 
        
        
      
    






####################################################  
               
    def load_cams(self):

            str_pipe1 = 'udpsrc port='
            #Take care of sapce on start
            str_pipe3 = ' buffer-size=60000000 caps= \"application/x-rtp, clock-rate=90000\" ! \
rtpjitterbuffer latency=1000 ! rtpmp2tdepay ! tsparse ! \
video/mpegts ! tsdemux name=demux ! queue ! h264parse ! \
video/x-h264, profile=main, alignment=au ! \
omxh264dec internal-entropy-buffers=5 low-latency=0 ! \
queue max-size-bytes=0 ! video/x-raw !  videoconvert ! omxh264enc  ! h264parse ! \
                   video/x-h264, profile=main  ! qtmux ! \
                filesink  location='

          
            str_pipe4 = ' ! application/x-rtp, clock-rate=90000,payload=96  \
                                 ! rtph264depay ! h264parse  ! avdec_h264 ! videoconvert ! x264enc   ! mpegtsmux ! \
                                filesink sync=False name=fsink location=' 


            jsonPath = os.getcwd()+'/cam.txt'
            with open(jsonPath, 'r') as f:
                for jsonObj in f:
                    CamDict = json.loads(jsonObj)
                    port = int(CamDict['port'].strip()) 
                    #dictionary_name[key] = value
                    self.cam_dict[port] = str_pipe1 + str(port)+str_pipe4          
                    
                
            

#if dict_1.get("a") is not None:
#self.player.get_by_name("file-source").set_property("location", filepath)
#####################################
    def  record(self, cam_port , clip_length, p_label_string):  
            pipe_prev = self.if_pipe_exist(cam_port)
            if pipe_prev is None :

                pipe_line = self.cam_dict.get(int(cam_port))
                if(self.Recordings_dict.get(int(cam_port)) is not None ): 
                    if(self.Recordings_dict.get(int(cam_port)) is True):              
                     print("\n already recording")
                    return
                pipe = pipe_line+self. get_file_name(cam_port, p_label_string)
                print("\n pipe >> ",pipe)
                BasePipeLine = Gst.parse_launch(pipe)  
                self.Cam_obj_dict[int(cam_port)] =  BasePipeLine
        
                bus = BasePipeLine.get_bus()
                bus.add_signal_watch() 
                bus.connect("message", self.on_message)
            else:
                BasePipeLine =  pipe_prev  
                fname = "new_"+self.get_file_name(cam_port, p_label_string)              
                BasePipeLine.get_by_name("fsink").set_property("location", fname)
         
            ret = BasePipeLine.set_state(Gst.State.PLAYING)
            if ret == Gst.StateChangeReturn.FAILURE:  
                print("Fail to start ")
                self.BasePipeLine.send_event(Gst.Event.new_eos())
                return
          
            self.Recordings_dict[int(cam_port)]=  True
        

            print("Recording started")
          
            th= Timer(int(clip_length), callback, args=(self,cam_port,))  

            th .start()       
##################################################################### self.cam_dict.get(int(cam_port))
    def  stop(self, cam_port ): 
           
            PipeObj = self.Cam_obj_dict.get(int(cam_port))
            
            if(PipeObj):
                PipeObj.send_event(Gst.Event.new_eos())  
                #PipeObj.set_state(Gst.State.NULL)
                PipeObj.get_state(Gst.CLOCK_TIME_NONE)
                del PipeObj

                del self.Cam_obj_dict[int(cam_port)] 
                self.Recordings_dict[int(cam_port)] = False

    def  pause(self, cam_port ): 
           
            PipeObj = self.Cam_obj_dict.get(int(cam_port))
            
            if(PipeObj):
                PipeObj.send_event(Gst.Event.new_eos())  
                PipeObj.set_state(Gst.State.READY)
                PipeObj.get_state(Gst.CLOCK_TIME_NONE)
               
    def  if_pipe_exist(self, cam_port ): 

          if  self.Cam_obj_dict: 
            PipeObj = self.Cam_obj_dict.get(int(cam_port))  
            if PipeObj:
                return PipeObj
       
          return    None             

######################################################################                 

    def on_message(self, bus, message):
             t = message.type
             print(t)
             if t == Gst.MessageType.ERROR:
                print("Error stooping ")
                self.stop()
             elif t == Gst.MessageType.EOS:
                  
                  print("End of stream ") 
                  self.stop()

             elif t == Gst.MessageType.STREAM_START:
                
                print("\n  TIMMER  started")
# create a thread timer object
#timer = Timer(3, task, args=('Hello world',))

    ######################################################################
    def  get_file_name(self , cam_port, p_label_string): 

      """ Return a string of the form yyyy-mm-dd-hms """
     
      today = datetime.today()
      y = str(today.year)
      m = str(today.month)
      d = str(today.day)
      h = str(today.hour)
      mi= str(today.minute)
      s = str(today.second)
      
      if (len(p_label_string) == 0 or p_label_string is None):
        p_label_string ="none"
      print("\n p_label_string is >> \n",p_label_string) 
 
    
      fname = str(cam_port)+"_"+p_label_string+"_"+m+d+y+"-"+h+mi+s+".mp4"

      return   fname.strip() #'%s-%s-%s-%s%s%s' %(y, m, d, h, mi, s)
    """ 

   str_pipe4 = ' ! application/x-rtp, clock-rate=90000,payload=96  \
                                 ! rtph264depay ! h264parse  ! avdec_h264 ! videoconvert ! x264enc   ! mpegtsmux ! \
                                filesink sync=False location='      
        """