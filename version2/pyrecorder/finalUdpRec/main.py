

# Version 5.0.1

from pathlib import Path

import threading
import ctypes
import time
import os
import gc
import gi

gi.require_version('Gst', '1.0')

from gi.repository import Gst
from datetime import datetime
Gst.init(None)

def callback(p_self, cam_id):
    print("stop called")
    p_self.rec_signal_stop=True


class AsyncRecorder(threading.Thread):

    def __init__(self):

           
            # calling superclass init
            threading.Thread.__init__(self)
          
             # Create GStreamer pipeline
            
            self.RecPipeLine = None


         


            ####################################
            
            
            
            self.Length =15
            self.terminate=False
            self.rec_status=False
            self.rec_signal_start=False
            self.rec_signal_stop=True
            self.udp_port = "5002"

            self.duration=1
            self.recordpipe = None
            self.count_ins=0
        
   


            
   
            
    def raise_exception(self):
        thread_id = self.get_id()
        res = ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id,
              ctypes.py_object(SystemExit))
        if res > 1:
            ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id, 0)
            print('Exception raise failure')
   
     

    def run(self):

        try:
            while not self.terminate :
                        
                        while self.rec_status is False and self.rec_signal_start is False :
                            pass
                        self.rec_status=True
                        self.rec_signal_stop=False
                       
                        clip_name = "recording/"+datetime.now().strftime("%Y-%m-%d_%H.%M.%S") + ".mp4"
                        str_rec_pipe = "udpsrc  port=5002 ! application/x-rtp, clock-rate=90000,payload=96  \
                                 ! rtph264depay ! h264parse  ! omxh264dec \
                                ! videoconvert \
                                !  omxh264enc target-bitrate=70000 prefetch-buffer=TRUE control-rate=2 gop-length=30 b-frames=0   ! mpegtsmux alignment=7 name=mux ! \
                                filesink sync=False location="+clip_name.strip()
                            


                        self.RecPipeLine  = Gst.parse_launch(str_rec_pipe  )
                        ret = self.RecPipeLine .set_state(Gst.State.PLAYING)
                        if ret == Gst.StateChangeReturn.FAILURE:
                                 print("\n ##################  ERROR : Unable to Record") 
                        while self.rec_signal_stop is False:
                            pass 
                        
                        if self.RecPipeLine is not None:                          
                            self.RecPipeLine.send_event(Gst.Event.new_eos())
                            self.RecPipeLine.set_state(Gst.State.NULL)
                        self.rec_status=False  
                        self.rec_signal_start =False
                        gc.collect()            
     
                   
             
        finally:
        
            print("finally: on Run >>> ")
     
     

##########################
# 
""" GRec = AsyncRecorder()
            # start the thread
GRec.start()      
 """



#########################################################################

import re
import threading as th  
######################################################################server 

import select
import socket
import time
import kv260_record


GRec = None

def server() -> None:
    global GRec
    host = "127.0.0.1" #socket.gethostname()
    port = 12003
    GRec = AsyncRecorder()
    GRec.start() 

  
    # create a TCP/IP socket
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        sock.setblocking(0)
        # bind the socket to the port
        print("\n host is  ",host)
        sock.bind((host, port))
        # listen for incoming connections
        sock.listen(5)
        print("Server started...")

        # sockets from which we expect to read
        inputs = [sock]
        outputs = []

        while inputs:
            # wait for at least one of the sockets to be ready for processing
            readable, writable, exceptional = select.select(inputs, outputs, inputs)

            for s in readable:
                if s is sock:
                    conn, addr = s.accept()
                    inputs.append(conn)
                else:
                    data = s.recv(1024)
                    if data:
                        print(data)
                        process_msg(  data.decode(errors='ignore') )

                        #process_msg(  data.decode() )
                    else:
                        inputs.remove(s)
                        s.close()





####################################################################
def process_msg(  p_msg ): 
         global GRec
         print("\n  >>>>>>>>>> \n",p_msg, "\n  >>>>>>>>>> \n")  
         word = p_msg.split(',') 
         if word[0] == "start": 
          print("\n Start")      
          port  = word[1].strip() 
          vlength  = word[2].strip()
          if word[3]:                                                           
           normal_string =re.sub("[^A-Z]", "", word[3].strip() ,0,re.IGNORECASE)
           label_string  = normal_string.strip()        
       
          GRec.rec_signal_start=True
        
          return


         elif word[0] == "stop": 
            print("\n Stop")
            if GRec:
             GRec.rec_signal_stop=True
             return
         elif word[0]:
          print("\n  this is C++ \n"+p_msg)    


if __name__ == "__main__":
 
    server() 
             
                  