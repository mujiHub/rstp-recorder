import sys

import gi

gi.require_version('GLib', '2.0')
gi.require_version('GObject', '2.0')
gi.require_version('Gst', '1.0')

location = '/dev/video0'
from gi.repository import Gst, GObject, GLib

pipeline = None
bus = None
message = None


# initialize gstreamer
Gst.init(sys.argv[1:])

# build pipeline
pipeline = Gst.parse_launch(
    "v4l2src device=" + location + " ! tee name=tee ! queue name=videoqueue ! deinterlace ! xvimagesink"
)

# start playing
pipeline.set_state(Gst.State.PLAYING)

# wait until EOS error
bus = pipeline.get_bus()
msg = bus.timed_pop_filtered(
    Gst.CLOCK_TIME_NONE,
    Gst.MessageType.ERROR | Gst.MessageType.EOS
)

# free resources
pipeline.set_state(Gst.State.NULL)