# Tasks.
# 1 udp pull Launch from service 
# 2 multi record 
# 3 create test source 
# 4 quality of recording 


import gi
gi.require_version('Gtk', '3.0')
gi.require_version('Gst', '1.0')

from gi.repository import Gtk
import threading
import time
from snif import *



class Udp_Puller():

    def __init__(self):        
        
        threading.Thread.__init__(self) 
        self.rtsp ="rtsp://192.168.29.171:8554/test" 

        self.PipeLine ="rtspsrc location=rtsp://192.168.29.171:8554/test/ ! udpsink host=127.0.0.1 port=5002  sync=false buffer-size=1400 blocksize=1400" 
        self.Gst_pipe = None
        self.start_pull()

    ###########################
 
    # on stream response 
    def on_message(self, bus, message):
             t = message.type
             #print(t)
             if t == Gst.MessageType.ERROR:
                print("Error stooping ")
                self.snif_stream(self.rtsp )
             elif t == Gst.MessageType.EOS:
                  
                  print("End of stream ") 
                  self.snif_stream(self.rtsp )
    ##############################

    def snif_stream(self, p_rtsp):
      print('Starting a  SNIFFER..')
      loop_on = True
      while loop_on:
        time.sleep(2)
        print("\n SNIF URL"+p_rtsp)
        ret= get_rtsp_status(p_rtsp)
        print("\n sniffer response is "+str(ret))
        if(ret==1):
           # _self.connect_Ip_cam()
            
            loop_on = False
            print("\n ###################  Stream FOUND EXITING SNIFF ##################### \n")    
        else:
            print("\n ###################  Stream NOT FOUND ##################### \n")  


        

    ################## start pulling stream 

    def start_pull(self):

        self.Gst_pipe  = Gst.parse_launch(self.PipeLine )
        bus = self.Gst_pipe.get_bus()
        bus.add_signal_watch()         
        bus.connect("message", self.on_message)


        ret = self.Gst_pipe.set_state(Gst.State.PLAYING)
        if ret == Gst.StateChangeReturn.FAILURE:
                print("\n ##################  ERROR : Unable to set the pipeline to the playing state")
                #sys.exit(1)
                self.snif_stream(self.rtsp )

if __name__ == "__main__":
   
   Gst.init(None)
   obj  = Udp_Puller()
   Gtk.main()

"""               
  
gst-launch-1.0 -v rtspsrc location="rtsp://embrill:embrill123@192.168.29.181:554/stream1" ! queue ! rtph264depay ! queue ! \
    h264parse ! queue ! omxh264dec ! video/x-raw, format=NV12 ! tee name=t ! queue ! \
    ivas_xmultisrc kconfig="/opt/xilinx/share/ivas/smartcam/facedetect/preprocess.json" ! queue ! \
    ivas_xfilter kernels-config="/opt/xilinx/share/ivas/smartcam/facedetect/aiinference.json" ! \
    ima.sink_master  ivas_xmetaaffixer name=ima ima.src_master ! fakesink  t. ! \
    queue max-size-buffers=1 leaky=2 ! ima.sink_slave_0 ima.src_slave_0 ! queue ! ivas_xfilter \
    kernels-config="/opt/xilinx/share/ivas/smartcam/facedetect/drawresult.json" ! queue ! \
    omxh264enc num-slices=8 periodicity-idr=240 cpb-size=500 gdr-mode=horizontal initial-delay=250 control-rate=low-latency \
    prefetch-buffer=true target-bitrate=25000 gop-mode=low-delay-p ! video/x-h264, alignment=nal ! rtph264pay  ! 
    udpsink buffer-size=60000000 host=192.168.29.5 port=5002 async=false max-lateness=-1     qos-dscp=60 max-bitrate=120000000 -v

       

gst-launch-1.0  rtspsrc location=rtsp://127.0.0.1:8554/snakeye/ ! udpsink host=192.168.100.28 port=5001  sync=false buffer-size=1400 blocksize=1400

gst-launch-1.0 -v  udpsrc port=5002 ! application/x-rtp, clock-rate=90000,payload=96  \
                                 ! rtph264depay ! h264parse  ! avdec_h264 ! videoconvert ! x264enc   ! mpegtsmux ! \
                                filesink sync=False location=test.mp4
 """