from pathlib import Path

import threading
import ctypes
import time
import os
import gc
import gi

gi.require_version('Gst', '1.0')

from gi.repository import Gst
from datetime import datetime
Gst.init(None)

location = '/dev/video0'

# rtsp://127.0.0.1:8554/snakeye
### https://gist.github.com/hum4n0id/cda96fb07a34300cdb2c0e314c14df0a#record-and-display-at-the-same-timequeue

class AsyncRecorder(threading.Thread):

    def __init__(self):

           
            # calling superclass init
            threading.Thread.__init__(self)
          
             # Create GStreamer pipeline
           
            self.load()


            ####################################
            
            
            
            self.Length =15
            self.terminate=False
            self.ON=True
            self.duration=1
            self.recordpipe = None
   ###############################################################
   # 
   # 

        
    def load(self ):
            
            Gst.init(None)
            self.pipeline = Gst.parse_launch( "rtspsrc location=rtsp://embrill:embrill123@192.168.29.181:554/stream1 \
! queue \
! rtph264depay \
! queue \
! h264parse \
! queue \
! omxh264dec \
! video/x-raw, format=NV12 \
! tee name=t \
! queue \
! ivas_xmultisrc kconfig=/opt/xilinx/share/ivas/smartcam/facedetect/preprocess.json \
! queue \
! ivas_xfilter kernels-config=/opt/xilinx/share/ivas/smartcam/facedetect/aiinference.json \
! ima.sink_master  ivas_xmetaaffixer name=ima ima.src_master \
! fakesink  t. \
! queue max-size-buffers=1 leaky=2 \
! ima.sink_slave_0 ima.src_slave_0 \
! queue \
! ivas_xfilter kernels-config=/opt/xilinx/share/ivas/smartcam/facedetect/drawresult.json \
! queue \
! perf \
! tee name=tr \
! queue max-size-buffers=1 leaky=2 \
! kmssink driver-name=xlnx plane-id=39 sync=false fullscreen-overlay=true ")
            self.recordpipe = None

            self.bus = self.pipeline.get_bus()
            self.pipeline.set_state(Gst.State.PLAYING)

    def probe_block(self, pad, buf):
            print('recording probe_block >')
          
            return True
            
   
            
    def raise_exception(self):
        thread_id = self.get_id()
        res = ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id,
              ctypes.py_object(SystemExit))
        if res > 1:
            ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id, 0)
            print('Exception raise failure')
        self.pipeline.set_state(Gst.State.NULL)    
     

    def run(self):

        try:
            while not self.terminate :
                        
                        # flush the bus
                        self.bus.set_flushing(True)
                        self.bus.set_flushing(False)
                        gc.collect()
     
                        msg = self.bus.timed_pop_filtered(int(self.duration) * Gst.SECOND, ( Gst.MessageType.ERROR))
                       
                        self.duration =	self.Length	
                   
                     					
                        if msg:
                            if msg.type == Gst.MessageType.ERROR:				
                                    print('RECORDING bus  on_error(): self.bus2 ', msg.parse_error())
                                    self.pipeline.set_state(Gst.State.NULL)
                                    self.load()
                                    continue
                            elif msg.type == Gst.MessageType.EOS:
                                    print("RECORDING bus  End-Of-Stream reached")
                                   
                        else: 

                        


                            
                                 

                            gc.collect()
                           	
                            self.start_record()	
             
        finally:
        
            print("finally: on Run >>> ")
            self.pipeline.set_state(Gst.State.NULL)     
           
    def start_record(self):
        # Filename (current time)
        
       
        filename = "rec/"+datetime.now().strftime("%Y-%m-%d_%H.%M.%S") + ".mp4"
        print(filename)
        self.recordpipe = Gst.parse_bin_from_description("queue  name=filequeue  \
         ! omxh264enc target-bitrate=70000 prefetch-buffer=TRUE control-rate=2 gop-length=30 b-frames=0  \
         ! queue ! video/x-h264, profile=main, alignment=au ! mpegtsmux alignment=7 name=mux \
            ! filesink location=" + filename+" sync=false async=0", True)
        self.pipeline.add(self.recordpipe)
        self.pipeline.get_by_name("tr").link(self.recordpipe)
       
        self.recordpipe.set_state(Gst.State.PLAYING)
        
 
    def stop_record(self):
        filequeue = self.recordpipe.get_by_name("filequeue")
        filequeue.get_static_pad("src").add_probe(Gst.PadProbeType.BLOCK_DOWNSTREAM, self.probe_block)
        self.pipeline.get_by_name("tr").unlink(self.recordpipe)
        filequeue.get_static_pad("sink").send_event(Gst.Event.new_eos())
        
        
        
        
        gc.collect()
     

##########################
# 
GRec = AsyncRecorder()
            # start the thread
GRec.start()    

""""

self.pipeline = Gst.parse_launch( "rtspsrc location=rtsp://embrill:embrill123@192.168.29.181:554/stream1 \
! queue \
! rtph264depay \
! queue \
! h264parse \
! queue \
! omxh264dec \
! video/x-raw, format=NV12 \
! tee name=t \
! queue \
! ivas_xmultisrc kconfig=/opt/xilinx/share/ivas/smartcam/facedetect/preprocess.json \
! queue \
! ivas_xfilter kernels-config=/opt/xilinx/share/ivas/smartcam/facedetect/aiinference.json \
! ima.sink_master  ivas_xmetaaffixer name=ima ima.src_master \
! fakesink  t. \
! queue max-size-buffers=1 leaky=2 \
! ima.sink_slave_0 ima.src_slave_0 \
! queue \
! ivas_xfilter kernels-config=/opt/xilinx/share/ivas/smartcam/facedetect/drawresult.json \
! queue \
! perf \
! tee name=tr \
! queue max-size-buffers=1 leaky=2 \
! kmssink driver-name=xlnx plane-id=39 sync=false fullscreen-overlay=true ")
       
self.recordpipe = Gst.parse_bin_from_description("queue  name=filequeue leaky=1 \
        ! omxh264enc \
                  !  filesink location=" + filename, True)       

"""