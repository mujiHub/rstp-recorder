
# Version 5.0.1

from pathlib import Path

import threading
import ctypes
import time
import os
import gc
import gi

gi.require_version('Gst', '1.0')

from gi.repository import Gst
from datetime import datetime
Gst.init(None)

location = '/dev/video0'

# rtsp://127.0.0.1:8554/snakeye
### https://gist.github.com/hum4n0id/cda96fb07a34300cdb2c0e314c14df0a#record-and-display-at-the-same-timequeue

class AsyncRecorder(threading.Thread):

    def __init__(self):

           
            # calling superclass init
            threading.Thread.__init__(self)
          
             # Create GStreamer pipeline
            self.pipeline = Gst.parse_launch("v4l2src device=" + location + " ! tee name=tr ! queue name=videoqueue ! deinterlace ! xvimagesink")
            self.recordpipe = None

            self.bus = self.pipeline.get_bus()
            self.pipeline.set_state(Gst.State.PLAYING)
     


            ####################################
            
            
            
            self.Length =15
            self.terminate=False
            self.ON=True
            self.duration=1
            self.recordpipe = None
            self.count_ins=0
        
    def on_eos(self, bus, msg):
        print('on_eos(): seeking to start of video')
        self.pipeline.seek_simple(
            Gst.Format.TIME,
            Gst.SeekFlags.FLUSH | Gst.SeekFlags.KEY_UNIT,
            0
        )

    def on_error(self, bus, msg):
        print('on_error():', msg.parse_error())   

    def probe_block(self, pad, buf):
            print('recording probe_block >')
          
            return True
            
   
            
    def raise_exception(self):
        thread_id = self.get_id()
        res = ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id,
              ctypes.py_object(SystemExit))
        if res > 1:
            ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id, 0)
            print('Exception raise failure')
        self.pipeline.set_state(Gst.State.NULL)    
     

    def run(self):

        try:
            while not self.terminate :
                        
                        # flush the bus
                        self.bus.set_flushing(True)
                        self.bus.set_flushing(False)
                        gc.collect()
     
                        msg = self.bus.timed_pop_filtered(int(self.duration) * Gst.SECOND, ( Gst.MessageType.ERROR))
                       
                        self.duration =	self.Length	
                   
                     					
                        if msg:
                            if msg.type == Gst.MessageType.ERROR:				
                                    print('RECORDING bus  on_error(): self.bus2 ', msg.parse_error())	
                            elif msg.type == Gst.MessageType.EOS:
                                        print("RECORDING bus  End-Of-Stream reached")
                        else: 

                        


                            
                                 

                            gc.collect()
                           	
                            self.start_record()	
             
        finally:
        
            print("finally: on Run >>> ")
            self.pipeline.set_state(Gst.State.NULL)     
           
    def start_record(self):
        # Filename (current time)
        
       
        filename = "recording/"+datetime.now().strftime("%Y-%m-%d_%H.%M.%S") + ".mp4"
        print(filename)
        self.recordpipe = Gst.parse_bin_from_description("queue name=filequeue leaky=1 ! jpegenc ! avimux ! filesink location=" + filename, True)
        self.pipeline.add(self.recordpipe)
        self.pipeline.get_by_name("tr").link(self.recordpipe)
       
        self.recordpipe.set_state(Gst.State.PLAYING)
        
 
    def stop_record(self):
        filequeue = self.recordpipe.get_by_name("filequeue")
        filequeue.get_static_pad("src").add_probe(Gst.PadProbeType.BLOCK_DOWNSTREAM, self.probe_block)
        self.pipeline.get_by_name("tr").unlink(self.recordpipe)
        filequeue.get_static_pad("sink").send_event(Gst.Event.new_eos())
        self.pipeline.remove(self.recordpipe)
        self.recordpipe.set_state(Gst.State.NULL) 
        del self.recordpipe
        
        print("Stopped recording")        
        
        
        
        self.recordpipe = None
        del self.recordpipe
        gc.collect()
     

##########################
# 
GRec = AsyncRecorder()
            # start the thread
GRec.start()      
""" 
    def record(self):
          
         
            gc.collect()
                                
                                        
                                       
#https://gist.github.com/hum4n0id/cda96fb07a34300cdb2c0e314c14df0a#record-and-display-at-the-same-timequeue
            if self.recordpipe is not None:
                    filequeue = self.recordpipe.get_by_name("filequeue")
                    filequeue.get_static_pad("src").add_probe(Gst.PadProbeType.BLOCK_DOWNSTREAM, self.probe_block)	
                    # Disconnect the recording pipe
                    self.mainPipe.get_by_name("tr").unlink(self.recordpipe)
                        # Send a termination event to trigger the save
                    filequeue.get_static_pad("sink").send_event(Gst.Event.new_eos())
                    # Clear the reference to the pipe
                    #self.recordpipe.set_state(Gst.State.NULL)
                    self.recordpipe = None
                    del self.recordpipe 
            if(self.SingleFile < 1):
                self.FileName = self.Folder+dt.datetime.now().strftime("%Y-%m-%d_%H.%M.%S") + ".mp4"
            
                self.recordpipe = Gst.parse_bin_from_description("queue name=filequeue leaky=1 !   x264enc ! video/x-h264, profile=baseline  ! matroskamux streamable=false !\
                  filesink location={} sync=False ".format(self.FileName), True)   
            else:
              self.recordpipe = Gst.parse_bin_from_description("queue name=filequeue leaky=1 !   x264enc ! video/x-h264, profile=baseline  ! matroskamux streamable=false !\
            filesink location={} sync=False append=true".format(self.FileName), True)
          
                
                
       
            # Add Bin To Line 
            self.mainPipe.add(self.recordpipe)
            # Connect to the main pipe
            self.mainPipe.get_by_name("tr").link(self.recordpipe)
            
            # Start passing data
            
            self.recordpipe.set_state(Gst.State.PLAYING)
 """