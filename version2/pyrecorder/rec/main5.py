
#########################################################################

import re
import threading as th  
######################################################################server 

import select
import socket
import time
import sys
from time import sleep
from threading import Thread
import gi

gi.require_version('GLib', '2.0')
gi.require_version('GObject', '2.0')
gi.require_version('Gst', '1.0')

from datetime import datetime
from gi.repository import Gst, GObject, GLib

bus = None
message = None
location = '/dev/video0'

# initialize gstreamer
Gst.init(sys.argv[1:])

from threading import Timer
GRec = None

def callback(p_self, msg):
    print("stop called")
    p_self.stop_record(msg)  
######################################################################
# custom thread class
class CustomThread(Thread):
    # override the run function
    def run(self):
       
        # build pipeline
    
        self.pipeline = Gst.parse_launch(
            "v4l2src device=" + location + " ! tee name=tee ! fakesink sync=false"
        )

        # start playing
        self.pipeline.set_state(Gst.State.PLAYING)

        # wait until EOS error
        bus = self.pipeline.get_bus()
        msg = bus.timed_pop_filtered(
            Gst.CLOCK_TIME_NONE,
            Gst.MessageType.ERROR | Gst.MessageType.EOS
        )

        # free resources
        self.pipeline.set_state(Gst.State.NULL)


    def start_record(self, label_string):
        
        # Filename (current time)
        filename = datetime.now().strftime("%Y-%m-%d_%H.%M.%S") + ".mp4"
        print(filename)
        self.recordpipe = Gst.parse_bin_from_description("queue name=filequeue ! jpegenc ! avimux ! filesink location=" + filename, True)
        self.pipeline.add(self.recordpipe)
        self.pipeline.get_by_name("tee").link(self.recordpipe)
        bus = self.recordpipe.get_bus()
        bus.add_signal_watch()
      
        self.recordpipe.set_state(Gst.State.PLAYING)
        th= Timer(int(20), callback, args=(self,"RinG It ",))  
        th .start()

    def stop_record(self, msg):
        print(msg)
        filequeue = self.recordpipe.get_by_name("filequeue")
        filequeue.get_static_pad("src").add_probe(Gst.PadProbeType.BLOCK_DOWNSTREAM, self.probe_block)
        self.pipeline.get_by_name("tee").unlink(self.recordpipe)
        filequeue.get_static_pad("sink").send_event(Gst.Event.new_eos())
        print("Stopped recording")



    def probe_block(self, pad, buf):
        print("blocked")
        return True    


####################################################################     
# 
#  
def server() -> None:
    global GRec
    host = "127.0.0.1" #socket.gethostname()
    port = 12003


  
    # create a TCP/IP socket
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        sock.setblocking(0)
        # bind the socket to the port
        print("\n host is  ",host)
        sock.bind((host, port))
        # listen for incoming connections
        sock.listen(5)
        print("Server started...")
        GRec = CustomThread()
            # start the thread
        GRec.start()
        print('Waiting for the thread to finish')
        

        # sockets from which we expect to read
        inputs = [sock]
        outputs = []

        while inputs:
            # wait for at least one of the sockets to be ready for processing
            readable, writable, exceptional = select.select(inputs, outputs, inputs)

            for s in readable:
                if s is sock:
                    conn, addr = s.accept()
                    inputs.append(conn)
                else:
                    data = s.recv(1024)
                    if data:
                        print(data)
                        process_msg(  data.decode(errors='ignore') )

                        #process_msg(  data.decode() )
                    else:
                        inputs.remove(s)
                        s.close()

        GRec.join()  

def process_msg(  p_msg ): 
         global GRec
         print("\n  >>>>>>>>>> \n",p_msg, "\n  >>>>>>>>>> \n")  
         word = p_msg.split(',') 
         if word[0] == "start": 
          print("\n Start")      
          port  = word[1].strip() 
          vlength  = word[2].strip()
          if word[3]:                                                           
           normal_string =re.sub("[^A-Z]", "", word[3].strip() ,0,re.IGNORECASE)
           label_string  = normal_string.strip()        
       
          GRec.start_record(label_string)
          
        
          return


         elif word[0] == "stop": 
            print("\n Stop")
            GRec.stop_record()
           
            return
         elif word[0]:
          print("\n  this is C++ \n"+p_msg)    


if __name__ == "__main__":
 
    server() 
             

            