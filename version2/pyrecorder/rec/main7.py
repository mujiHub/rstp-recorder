
#########################################################################

import re
import threading as th  
######################################################################server 
import gc
import select
import socket
import time
import sys
from time import sleep
from threading import Thread
import gi

gi.require_version('GLib', '2.0')
gi.require_version('GObject', '2.0')
gi.require_version('Gst', '1.0')

from datetime import datetime
from gi.repository import Gst, GObject, GLib

bus = None
message = None
location = '/dev/video0'



from threading import Timer
GRec = None

def callback(p_self, msg):
    print("stop called")
    p_self.stop_record(msg)  
######################################################################
# custom thread class
class CustomThread(Thread):
    # override the run function
 
      

    def run(self):
       
        # build pipeline
        # initialize gstreamer
        
        self.pipeline = Gst.parse_launch(
            "rtspsrc location=rtsp://embrill:embrill123@192.168.29.181:554/stream1 \
! queue \
! rtph264depay \
! queue \
! h264parse \
! queue \
! omxh264dec \
! video/x-raw, format=NV12 \
! tee name=t \
! queue \
! ivas_xmultisrc kconfig=/opt/xilinx/share/ivas/smartcam/facedetect/preprocess.json \
! queue \
! ivas_xfilter kernels-config=/opt/xilinx/share/ivas/smartcam/facedetect/aiinference.json \
! ima.sink_master  ivas_xmetaaffixer name=ima ima.src_master \
! fakesink  t. \
! queue max-size-buffers=1 leaky=2 \
! ima.sink_slave_0 ima.src_slave_0 \
! queue \
! ivas_xfilter kernels-config=/opt/xilinx/share/ivas/smartcam/facedetect/drawresult.json \
! queue \
! perf \
! tee name=tu \
! queue \
! kmssink driver-name=xlnx plane-id=39 sync=false fullscreen-overlay=true tu. \
! queue ! omxh264enc num-slices=8 periodicity-idr=240 cpb-size=500 gdr-mode=horizontal initial-delay=250 \
control-rate=low-latency prefetch-buffer=true target-bitrate=25000 gop-mode=low-delay-p ! video/x-h264, \
alignment=nal ! rtph264pay  ! \
udpsink buffer-size=60000000 host=192.168.29.172 port=5002 async=false max-lateness=-1 \
    qos-dscp=60 max-bitrate=120000000 "
        )

        # start playing
        self.pipeline.set_state(Gst.State.PLAYING)

        # wait until EOS error
        bus = self.pipeline.get_bus()
        msg = bus.timed_pop_filtered(
            Gst.CLOCK_TIME_NONE,
            Gst.MessageType.ERROR | Gst.MessageType.EOS
        )

        # free resources
        self.pipeline.set_state(Gst.State.NULL)






    def start_record(self, label_string):
        


        # Filename (current time)
        filename = datetime.now().strftime("%Y-%m-%d_%H.%M.%S") + ".mp4"
        print(filename)

        self.recordpipe = Gst.parse_bin_from_description("queue  name=filequeue  \
          omxh264enc  \
         ! queue ! video/x-h264, profile=main, alignment=au ! mpegtsmux alignment=7 name=mux \
            ! filesink location=" + filename+" sync=false", True)
        self.pipeline.add(self.recordpipe)
        self.pipeline.get_by_name("tu").link(self.recordpipe)
        self.recordpipe.set_state(Gst.State.PLAYING)
        th= Timer(int(20), callback, args=(self,"RinG It ",))  
        th .start()
        th.join()



    def stop_record(self, msg):
        print(msg)
        filequeue = self.recordpipe.get_by_name("filequeue")
        filequeue.get_static_pad("src").add_probe(Gst.PadProbeType.BLOCK_DOWNSTREAM, self.probe_block)
        self.pipeline.get_by_name("tu").unlink(self.recordpipe)
        filequeue.get_static_pad("sink").send_event(Gst.Event.new_eos())
        print("Stopped recording")
       # self.recordpipe.set_state(Gst.State.NULL)
       # self.recordpipe.get_state(Gst.CLOCK_TIME_NONE)
        
        




    def probe_block(self, pad, buf):
        print("blocked")
        gc.collect()
       
        return True    


####################################################################     
# 
#  
def server() -> None:
    global GRec
    host = "127.0.0.1" #socket.gethostname()
    port = 12003


  
    # create a TCP/IP socket
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        sock.setblocking(0)
        # bind the socket to the port
        print("\n host is  ",host)
        sock.bind((host, port))
        # listen for incoming connections
        sock.listen(5)
        print("Server started...")
        GRec = CustomThread()
            # start the thread
        GRec.start()
        print('Waiting for the thread to finish')
        

        # sockets from which we expect to read
        inputs = [sock]
        outputs = []

        while inputs:
            # wait for at least one of the sockets to be ready for processing
            readable, writable, exceptional = select.select(inputs, outputs, inputs)

            for s in readable:
                if s is sock:
                    conn, addr = s.accept()
                    inputs.append(conn)
                else:
                    data = s.recv(1024)
                    if data:
                        print(data)
                        process_msg(  data.decode(errors='ignore') )

                        #process_msg(  data.decode() )
                    else:
                        inputs.remove(s)
                        s.close()

        GRec.join()  

def process_msg(  p_msg ): 
         global GRec
         print("\n  >>>>>>>>>> \n",p_msg, "\n  >>>>>>>>>> \n")  
         word = p_msg.split(',') 
         if word[0] == "start": 
          print("\n Start")      
          port  = word[1].strip() 
          vlength  = word[2].strip()
          if word[3]:                                                           
           normal_string =re.sub("[^A-Z]", "", word[3].strip() ,0,re.IGNORECASE)
           label_string  = normal_string.strip()        
       
          GRec.start_record(label_string)
          
        
          return


         elif word[0] == "stop": 
            print("\n Stop")
            GRec.stop_record()
           
            return
         elif word[0]:
          print("\n  this is C++ \n"+p_msg)    


if __name__ == "__main__":
    Gst.init(sys.argv[1:])
    server() 
             

            