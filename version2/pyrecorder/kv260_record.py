
import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst
from gi.repository import GObject, Gst, GLib
from threading import Timer
import json
from datetime import datetime




REC_STATE = False
location = '/dev/video0'  

   ##########################
     # 
def callback(p_self):
    print("stop called")
    p_self.stop_record()  



class CRecord():

    

    def __init__(self, port):

        Gst.init(None)      
        
        self.Port = port
        self.cam_dict = {}
        self.Recordings_dict ={}
        self.Cam_obj_dict ={}
        self.load_cams()
        self.loop = GLib.MainLoop()  
        

       
        # parse file
      
 
        
        
      






####################################################  
               
    def load_cams(self):

           # Create GStreamer pipeline
         
     
        self.pipeline = Gst.parse_launch("rtspsrc location=rtsp://embrill:embrill123@192.168.29.181:554/stream1 \
! queue \
! rtph264depay \
! queue \
! h264parse \
! queue \
! omxh264dec \
! video/x-raw, format=NV12 \
! tee name=t \
! queue \
! ivas_xmultisrc kconfig=/opt/xilinx/share/ivas/smartcam/facedetect/preprocess.json \
! queue \
! ivas_xfilter kernels-config=/opt/xilinx/share/ivas/smartcam/facedetect/aiinference.json \
! ima.sink_master  ivas_xmetaaffixer name=ima ima.src_master \
! fakesink  t. \
! queue max-size-buffers=1 leaky=2 \
! ima.sink_slave_0 ima.src_slave_0 \
! queue \
! ivas_xfilter kernels-config=/opt/xilinx/share/ivas/smartcam/facedetect/drawresult.json \
! queue \
! perf \
! tee name=tr \
! queue max-size-buffers=1 leaky=2 \
! kmssink driver-name=xlnx plane-id=39 sync=false fullscreen-overlay=true ")
    
        # Create bus to get events from GStreamer pipeline
        bus = self.pipeline.get_bus()
        bus.add_signal_watch()
        bus.connect('message::eos', self.on_eos)
        bus.connect('message::error', self.on_error)
        self.pipeline.set_state(Gst.State.PLAYING)
        
     
        
        
       
         

    def on_eos(self, bus, msg):
        print('on_eos(): seeking to start of video')
        self.pipeline.seek_simple(
            Gst.Format.TIME,
            Gst.SeekFlags.FLUSH | Gst.SeekFlags.KEY_UNIT,
            0
        )

    def on_error(self, bus, msg):
        print('on_error():', msg.parse_error())

    def start_record(self, lbl):
        # Filename (current time)
        filename = datetime.now().strftime("%Y-%m-%d_%H.%M.%S") + ".mp4"
        print(filename)
        self.recordpipe = Gst.parse_bin_from_description("queue name=filequeue ! videoconvert \
        ! omxh265enc \
        !   video/x-h265, profile=main, alignment=au ! mpegtsmux alignment=7   name=mux \
        ! filesink location=" + filename, True)
        self.pipeline.add(self.recordpipe)
        self.pipeline.get_by_name("tr").link(self.recordpipe)
        self.recordpipe.set_state(Gst.State.PLAYING)
        
        th= Timer(int(20), callback, args=(self,))  
        th .start()
          
        self.loop.run()

    def stop_record(self):
        filequeue = self.recordpipe.get_by_name("filequeue")
        filequeue.get_static_pad("src").add_probe(Gst.PadProbeType.BLOCK_DOWNSTREAM, self.probe_block)
        self.pipeline.get_by_name("tr").unlink(self.recordpipe)
        filequeue.get_static_pad("sink").send_event(Gst.Event.new_eos())
        self.recordpipe.send_event(Gst.Event.new_eos())
        self.recordpipe.set_state(Gst.State.NULL)
        self.recordpipe.get_state(Gst.CLOCK_TIME_NONE)
        
        del self.recordpipe
        print("Stopped recording")    
        self.loop.quit()
    ###################################
    
       

#######################################################################

    def probe_block(self, pad, buf):
        print("blocked")
        return True


######################################################################                 
   