import os
import time
import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst
from threading import Timer
import json
from datetime import datetime




REC_STATE = False


def callback(p_self, cam_id):
    print("stop called")
    p_self.stop_record()
   

class CRecord():

    

    def __init__(self, port):

        Gst.init(None)
        self.Port = port
        self.cam_dict = {}
        self.Recordings_dict ={}
        self.Cam_obj_dict ={}
        self.load_cams()
        

       
        # parse file
      
 
        
        
      
    





####################################################  
               
    def load_cams(self):

            str_pipe1 = ' udpsrc port='
        
            self.str_pipe3 = 'udpsrc port=5002 buffer-size=60000000 \
caps="application/x-rtp, media=video, clock-rate=90000, \
payload=96, encoding-name=H264" ! rtpjitterbuffer latency=7 \
! rtph264depay ! h264parse ! video/x-h264, alignment=nal \
! omxh264dec low-latency=1 internal-entropy-buffers=5 \
! video/x-raw !  videoconvert ! tee name=tee ! queue max-size-bytes=0 ! fakesink'

          
            str_pipe4 = 'udpsrc port=5002 ! application/x-rtp, clock-rate=90000,payload=96 ! \
           rtph264depay ! h264parse  ! avdec_h264 ! videoconvert  ! \
             tee name=tee ! fakesink' 


              # Create GStreamer pipeline
            """  self.GST_Pipe_Line = Gst.parse_launch(self.str_pipe3)
            bus = self.GST_Pipe_Line.get_bus()
            bus.add_signal_watch() 
            bus.connect("message", self.on_message)
            self.GST_Pipe_Line .set_state(Gst.State.PLAYING) """

                    
                
       

#####################################
    def  record(self, cam_port , clip_length, p_label_string):  
            
            global REC_STATE 
            if REC_STATE :
                print("\n Already Recording \n")
                return
            REC_STATE = True
            self.start_record( self.get_file_name(cam_port, p_label_string), clip_length, cam_port )
             

            
          

            """   omxh264enc  ! h264parse ! \
                   video/x-h264, profile=main  ! qtmux ! \
                filesink  location=test5.mp4 """
#######################################################################

    def start_record(self, p_filename, p_clip_length, p_cam_port):
            self.GST_Pipe_Line = Gst.parse_launch(self.str_pipe3)            
            self.GST_Pipe_Line .set_state(Gst.State.PLAYING)  

            #self.recordpipe = Gst.parse_bin_from_description("queue name=filequeue ! x264enc ! mpegtsmux ! filesink sync=False  location=" + p_filename, True)
            self.recordpipe = Gst.parse_bin_from_description("queue name=filequeue ! \
                videoconvert ! omxh264enc  ! h264parse ! \
                   video/x-h264, profile=main  ! mpegtsmux ! \
                filesink  sync=False location=" + p_filename, True)
            self.GST_Pipe_Line.add(self.recordpipe)
            self.GST_Pipe_Line.get_by_name("tee").link(self.recordpipe)
            self.recordpipe.set_state(Gst.State.PLAYING)
            th= Timer(int(p_clip_length)+4, callback, args=(self,p_cam_port,))  

            th .start()

    def stop_record(self):
        filequeue = self.recordpipe.get_by_name("filequeue")
        filequeue.get_static_pad("src").add_probe(Gst.PadProbeType.BLOCK_DOWNSTREAM, self.probe_block)
        self.GST_Pipe_Line.get_by_name("tee").unlink(self.recordpipe)
        filequeue.get_static_pad("sink").send_event(Gst.Event.new_eos())
        print("Stopped recording")
        global REC_STATE 
        REC_STATE =False
        time.sleep(2)
        self.GST_Pipe_Line .set_state(Gst.State.NULL) 


######################################################################                 
    def probe_block(self, pad, buf):
        print("blocked")
        return True
    def on_message(self, bus, message):
             t = message.type
             print(t)
             if t == Gst.MessageType.ERROR:
                print("Error stooping ")
                self.stop()
             elif t == Gst.MessageType.EOS:
                  
                  print("End of stream ") 
                  self.stop()

             elif t == Gst.MessageType.STREAM_START:
                
                print("\n  TIMMER  started")
# create a thread timer object
#timer = Timer(3, task, args=('Hello world',))

    ######################################################################
    def  get_file_name(self , cam_port, p_label_string): 

      """ Return a string of the form yyyy-mm-dd-hms """
     
      today = datetime.today()
      y = str(today.year)
      m = str(today.month)
      d = str(today.day)
      h = str(today.hour)
      mi= str(today.minute)
      s = str(today.second)
      
      if (len(p_label_string) == 0 or p_label_string is None):
        p_label_string ="none"
      print("\n p_label_string is >> \n",p_label_string) 
 
    
      fname ="rec/"+ str(cam_port)+"_"+p_label_string+"_"+m+d+y+"-"+h+mi+s+".mp4"

      return   fname.strip() #'%s-%s-%s-%s%s%s' %(y, m, d, h, mi, s)



    """ 

gst-launch-1.0 -e udpsrc port=5200 buffer-size=60000000 \
caps="application/x-rtp, media=video, clock-rate=90000, \
payload=96, encoding-name=H264" ! rtpjitterbuffer latency=7 \
! rtph264depay ! h264parse ! video/x-h264, alignment=nal \
! omxh264dec low-latency=1 internal-entropy-buffers=5 \
! video/x-raw !  videoconvert ! omxh264enc  ! h264parse ! \
                   video/x-h264, profile=main  ! qtmux ! \
                filesink  location=test5.mp4

####################################
   gst-launch-1.0 -v rtspsrc location="rtsp://embrill:embrill123@192.168.29.181:554/stream1" ! \
queue ! rtph264depay ! queue ! h264parse ! queue ! omxh264dec ! video/x-raw, format=NV12 ! \
tee name=t ! queue ! ivas_xmultisrc kconfig="/opt/xilinx/share/ivas/smartcam/facedetect/preprocess.json" \
! queue ! ivas_xfilter kernels-config="/opt/xilinx/share/ivas/smartcam/facedetect/aiinference.json" ! \
ima.sink_master  ivas_xmetaaffixer name=ima ima.src_master ! fakesink  t. ! \
queue max-size-buffers=1 leaky=2 ! ima.sink_slave_0 ima.src_slave_0 ! queue ! \
ivas_xfilter kernels-config="/opt/xilinx/share/ivas/smartcam/facedetect/drawresult.json" ! \
queue ! omxh264enc num-slices=8 periodicity-idr=240 cpb-size=500 gdr-mode=horizontal initial-delay=250 \
control-rate=low-latency prefetch-buffer=true target-bitrate=25000 gop-mode=low-delay-p ! video/x-h264, \
alignment=nal ! rtph264pay  ! \
udpsink buffer-size=60000000 host=192.168.29.5 port=5200 async=false max-lateness=-1 \
    qos-dscp=60 max-bitrate=120000000 -v




gst-launch-1.0 -v  udpsrc port=5002 ! application/x-rtp, clock-rate=90000,payload=96  \
                                 ! rtph264depay ! h264parse  ! omxh264dec ! videoconvert ! x264enc   ! mpegtsmux ! \
                                filesink sync=False location=test.mp4



                                 gst-launch-1.0 -v rtspsrc location="rtsp://embrill:embrill123@192.168.29.181:554/stream1" ! \
queue ! rtph264depay ! queue ! h264parse ! queue ! omxh264dec ! video/x-raw, format=NV12 ! \
tee name=t ! queue ! ivas_xmultisrc kconfig="/opt/xilinx/share/ivas/smartcam/facedetect/preprocess.json" \
! queue ! ivas_xfilter kernels-config="/opt/xilinx/share/ivas/smartcam/facedetect/aiinference.json" ! \
ima.sink_master  ivas_xmetaaffixer name=ima ima.src_master ! fakesink  t. ! \
queue max-size-buffers=1 leaky=2 ! ima.sink_slave_0 ima.src_slave_0 ! queue ! \
ivas_xfilter kernels-config="/opt/xilinx/share/ivas/smartcam/facedetect/drawresult.json" ! \
queue ! omxh264enc num-slices=8 periodicity-idr=240 cpb-size=500 gdr-mode=horizontal initial-delay=250 \
control-rate=low-latency prefetch-buffer=true target-bitrate=25000 gop-mode=low-delay-p ! video/x-h264, \
alignment=nal ! rtph264pay  ! \
udpsink buffer-size=60000000 host=192.168.29.172 port=5002 async=false max-lateness=-1 \
    qos-dscp=60 max-bitrate=120000000  t. \
! queue \
! perf \
! kmssink driver-name=xlnx plane-id=39 sync=false fullscreen-overlay=true -v
 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 
 https://xilinx.github.io/kria-apps-docs/kv260/2022.1/build/html/docs/aibox-reid/docs/app_deployment_aib.html
 https://xilinx.github.io/VVAS/1.1/build/html/docs/Embedded/Tutorials/MultiChannelML.html

@@@@@@@@@@@@@@@@@@@@@@@@

gst-launch-1.0 rtspsrc location="rtsp://embrill:embrill123@192.168.29.181:554/stream1" \
! queue \
! rtph264depay \
! queue \
! h264parse \
! queue \
! omxh264dec \
! video/x-raw, format=NV12 \
! tee name=t \
! queue \
! ivas_xmultisrc kconfig="/opt/xilinx/share/ivas/smartcam/facedetect/preprocess.json" \
! queue \
! ivas_xfilter kernels-config="/opt/xilinx/share/ivas/smartcam/facedetect/aiinference.json" \
! ima.sink_master  ivas_xmetaaffixer name=ima ima.src_master \
! fakesink  t. \
! queue max-size-buffers=1 leaky=2 \
! ima.sink_slave_0 ima.src_slave_0 \
! queue \
! ivas_xfilter kernels-config="/opt/xilinx/share/ivas/smartcam/facedetect/drawresult.json" \
! queue \
! perf \
! kmssink driver-name=xlnx plane-id=39 sync=false fullscreen-overlay=true t. \
! queue ! omxh264enc num-slices=8 periodicity-idr=240 cpb-size=500 gdr-mode=horizontal initial-delay=250 \
control-rate=low-latency prefetch-buffer=true target-bitrate=25000 gop-mode=low-delay-p ! video/x-h264, \
alignment=nal ! rtph264pay  ! \
udpsink buffer-size=60000000 host=192.168.29.172 port=5002 async=false max-lateness=-1 \
    qos-dscp=60 max-bitrate=120000000

####################################################################################################################

gst-launch-1.0 v4l2src device=/dev/video0 io-mode=4 ! \
video/x-raw,format=NV12,width=3840,height=2160,framerate=60/1 ! \
omxh265enc qp-mode=auto gop-mode=basic gop-length=60 b-frames=0 \
target-bitrate=60000 num-slices=8 control-rate=constant prefetch-
buffer=true \
low-bandwidth=false filler-data=true cpb-size=1000 initial-delay=500 ! \
queue ! video/x-h265, profile=main, alignment=au ! mpegtsmux alignment=7
name=mux ! \
filesink location="/run/media/sda/test.ts"


        """