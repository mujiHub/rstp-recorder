#!/usr/bin/env python3

'''
https://github.com/GStreamer/gst-python/blob/master/examples/dynamic_src.py

Simple example to demonstrate dynamically adding and removing source elements
to a playing pipeline.




gst-launch-1.0 -e udpsrc port=5002 buffer-size=60000000 \
caps="application/x-rtp, media=video, clock-rate=90000, \
payload=96, encoding-name=H264" ! rtpjitterbuffer latency=7 \
! rtph264depay ! h264parse ! video/x-h264, alignment=nal \
! omxh264dec low-latency=1 internal-entropy-buffers=5 \
! video/x-raw !  videoconvert ! omxh264enc  ! h264parse ! \
                   video/x-h264, profile=main  ! qtmux ! \
                filesink  location=test5.mp4


'''

from datetime import datetime
import sys
import random

import gi
gi.require_version('Gst', '1.0')
gi.require_version('GLib', '2.0')
gi.require_version('GObject', '2.0')
from gi.repository import GLib, GObject, Gst

class ProbeData:
    def __init__(self, pipe, src):
        self.pipe = pipe
        self.src = src

def  get_file_name( cam_port, p_label_string): 

      """ Return a string of the form yyyy-mm-dd-hms """
     
      today = datetime.today()
      y = str(today.year)
      m = str(today.month)
      d = str(today.day)
      h = str(today.hour)
      mi= str(today.minute)
      s = str(today.second)
      
      if (len(p_label_string) == 0 or p_label_string is None):
        p_label_string ="none"
      print("\n p_label_string is >> \n",p_label_string) 
 
    
      fname ="rec/"+ str(cam_port)+"_"+p_label_string+"_"+m+d+y+"-"+h+mi+s+".mp4"

      return   fname.strip() #'%s-%s-%s-%s%s%s' %(y, m, d, h, mi, s)
def bus_call(bus, message, loop):
    t = message.type
    if t == Gst.MessageType.EOS:
        sys.stdout.write("End-of-stream\n")
        loop.quit()
    elif t == Gst.MessageType.ERROR:
        err, debug = message.parse_error()
        sys.stderr.write("Error: %s: %s\n" % (err, debug))
        loop.quit()
    return True

def dispose_src_cb(src):
    src.set_state(Gst.State.NULL)

def probe_cb(pad, info, pdata):
    peer = pad.get_peer()
    pad.unlink(peer)
    pdata.pipe.remove(pdata.src)
    # Can't set the state of the src to NULL from its streaming thread
    GLib.idle_add(dispose_src_cb, pdata.src)

    pdata.src = Gst.ElementFactory.make('filesink')
    pdata.src.props.location =  get_file_name("5002","label")
    pdata.pipe.add(pdata.src)
    srcpad = pdata.src.get_static_pad ("sink")
    srcpad.link(peer)
    pdata.src.sync_state_with_parent()

    GLib.timeout_add_seconds(5, timeout_cb, pdata)

    return Gst.PadProbeReturn.REMOVE
    

""" gst-launch-1.0 -e udpsrc port=5002 buffer-size=60000000 \
caps="application/x-rtp, media=video, clock-rate=90000, \
payload=96, encoding-name=H264" ! rtpjitterbuffer latency=7 \
! rtph264depay ! h264parse ! video/x-h264, alignment=nal \
! omxh264dec low-latency=1 internal-entropy-buffers=5 \
! video/x-raw !  videoconvert ! omxh264enc  ! h264parse ! \
                   video/x-h264, profile=main  ! qtmux ! \
                filesink  location=test5.mp4

     str_pipe4 = ' ! application/x-rtp, clock-rate=90000,payload=96  \
                                 ! rtph264depay ! h264parse  ! avdec_h264 ! videoconvert ! x264enc   ! mpegtsmux ! \
                                filesink sync=False location='       
 """


def timeout_cb(pdata):
    srcpad = pdata.src.get_static_pad('sink')
    srcpad.add_probe(Gst.PadProbeType.IDLE, probe_cb, pdata)
    return GLib.SOURCE_REMOVE

def main(args):
    GObject.threads_init()
    Gst.init(None)

    pipe = Gst.Pipeline.new('dynamic')

    src = Gst.ElementFactory.make('udpsrc')
    src.set_property("port", 5002)

    caps = Gst.Caps.from_string("application/x-rtp, clock-rate=90000,payload=96")
    filter = Gst.ElementFactory.make("capsfilter", "filter")
    filter.set_property("caps", caps)



    depayload = Gst.ElementFactory.make("rtph264depay")
    parser = Gst.ElementFactory.make("h264parse")
    decoder = Gst.ElementFactory.make("avdec_h264")
    conv = Gst.ElementFactory.make("videoconvert")
    enc = Gst.ElementFactory.make("x264enc")
    mux = Gst.ElementFactory.make("mpegtsmux")



    sink = Gst.ElementFactory.make('filesink')
    sink.set_property("sync", False)
    sink.set_property("location", get_file_name("5002","label"))




    pipe.add(src)

    pipe.add(filter)
    src.link_filtered(depayload, caps)
    pipe.add(depayload)
    pipe.add(parser)
    pipe.add(decoder)
    pipe.add(conv)
    pipe.add(enc)
    pipe.add(mux)


    pipe.add( sink)
    ####################  inter link elements
    src.link(filter)
    filter.link(depayload)
    depayload.link(parser)
    parser.link(decoder)
    decoder.link(conv)
    conv.link(enc)
    enc.link(mux)
    mux.link(sink)

    pdata = ProbeData(pipe, sink)

    loop = GObject.MainLoop()

    GLib.timeout_add_seconds(10, timeout_cb, pdata)

    bus = pipe.get_bus()
    bus.add_signal_watch()
    bus.connect ("message", bus_call, loop)
    
    # start play back and listen to events
    pipe.set_state(Gst.State.PLAYING)
    try:
      loop.run()
    except:
      pass
    
    # cleanup
    pipe.set_state(Gst.State.NULL)

if __name__ == '__main__':
    sys.exit(main(sys.argv))